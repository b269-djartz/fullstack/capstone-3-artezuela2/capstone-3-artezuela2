import {useState, useEffect,useContext} from 'react';
import UserCard from "../components/UserCard";
import UserContext from '../UserContext';

export default function Users() {

	const {user, setUser} = useContext(UserContext);

	const [users, setUsers] = useState([]);

	

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/all`,{
			method: 'POST',
            headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data.length > 0){
				console.log(data)
				setUsers(data.map(user => {
				return(
					<UserCard key={user._id} user={user} />
				)
			}))
			} else if (data.userNotAdmin) {
				alert("User must be ADMIN to access this")
			}
			
		})
	}, [])

	return (
		<>
		{users} 
		</>
	) 
}

