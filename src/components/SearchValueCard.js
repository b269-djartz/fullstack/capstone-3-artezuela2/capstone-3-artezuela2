import {useState,useEffect,useContext} from 'react';
import {Link} from "react-router-dom"
import UserContext from '../UserContext'

import { Button, Row, Col, Card } from 'react-bootstrap';



export default function SearchValueCard({searchValue, handleChange}) {

    const {title,author,genre,stock,description,price,_id} = searchValue;


    const {user} = useContext(UserContext)

    const [isBookInWishlist, setIsBookInWishlist] = useState(false);
    
    useEffect(() => {
    
    fetch(`${process.env.REACT_APP_API_URL}/users/checkUsersWishlist`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },body: JSON.stringify({
        bookId: _id
      }),
    })
    .then(res => res.json())
    .then(data => {
      if (data=== true) {
        
        setIsBookInWishlist(true)

      } else {
      console.log(data);
      }
      
    })
  }, [])

    // const isBookInWishlist = user && user.wishlist && user.wishlist.includes(title);


return (
  <Col className="mb-3 mt-3">
    <Card className="book-card p-0">
      <Card.Img  variant="top" src={`https://picsum.photos/200?${title}-${author}`} />
      <Card.Body className="text-center">
        <Card.Title className="_card_title" as={Link} to={`/books/${_id}`} onClick={handleChange} >
          <h4 >{title}</h4>
        </Card.Title> 
        <Card.Subtitle>Author</Card.Subtitle>
        <Card.Text>{author}</Card.Text>
        <Card.Subtitle>Price</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        {(() => {
          if (user.id && isBookInWishlist) {
            return (
              <>
                <Card.Subtitle>This Book is in your wishlist</Card.Subtitle>
              </>
            );
          }
        })()}
        { user.isAdmin ? 

          <>
          <Button onClick={handleChange} as={Link} to={`/books/${_id}/update`}> Edit book </Button>
          <Button onClick={handleChange} variant="danger" as={Link} to={`/books/${_id}/archive`}> Archive </Button>
          </> :
          <span></span>

        }
      </Card.Body>
    </Card>
  </Col>
  )
}
// [S50 ACTIVITY END]
